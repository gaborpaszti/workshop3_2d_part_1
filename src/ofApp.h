#pragma once
//#include "ofxXml.h"


#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		bool colide(ofPlanePrimitive a, ofPlanePrimitive b);   /// collision function

		ofPlanePrimitive plane, player1, player2;

		/// 1st smaller rectangle
		int player1_x; // = player1.getX;
		int player1_y; // = player1.getY;

		/// 2st smaller rectangle
		int player2_x; // = player2.getX;
		int player2_y; // = player2.getY;

		int r, g, b;

		int xVelocity, yVelocity;

		ofImage image;


};
