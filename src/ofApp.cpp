#include "ofApp.h"
#include <iostream>


//--------------------------------------------------------------
void ofApp::setup(){

	image.load("boom.png");

	/// This variables are helping to change the player colours when they collide
	r = 255;
	g = 0;
	b = 255;

	xVelocity = 0;
	yVelocity = 0;



	/// Main plane - the big one covering the screen
	plane.set(640, 480);               ///dimensions for width and height in pixels
	plane.setPosition(320, 240, 0);    /// position in x y z
	plane.setResolution(2, 2);        /// this resolution (as columns and rows) is enough

	/*/// 1st smaller rectangle
	int player1_x = player1.getX;
	int player1_x = player1.getY;*/

	//// 1st smaller rectangle - the player 1
	//// The active primitive
	player1_x = 0;                                  ///player1.getX;
	player1_y = 0;                                 /// player1.getY;
	player1.set(50, 50);                           ///dimensions for width and height in pixels
	player1.setPosition(player1_x, player1_y, 0); /// position in x y z
	player1.setResolution(2, 2);                 /// this resolution (as columns and rows) is enough

	//// 2nd smaller rectangle - the player 2
	//// The passive primitive
	player2_x = 250;                                  /// player2.getX;
	player2_y = 250;                                 /// player2.getY;
	player2.set(50, 50);                            ///dimensions for width and height in pixels
	player2.setPosition(player2_x, player2_y, 0);  /// position in x y z
	player2.setResolution(2, 2);                  /// this resolution (as columns and rows) is enough

	
	
	
	 


}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){

	ofPushMatrix();
	ofSetColor(0, 0, 0);
	plane.draw();

	ofSetColor(r, g, 0);
	player1.setPosition(xVelocity + 25, yVelocity + 25, 0); /// position in x y z
	player1.draw();

	ofSetColor(0, g, b); 
	player2.setPosition(player2_x, player2_y, 0); /// position in x y z
	player2.draw();
	ofPopMatrix();


	//// The logic to detect collision - to copy it to the function
	bool collision = false;

	if (player1_x > player2_x - 75 &&
		player1_x < player2_x + 30 &&
		player1_y > player2_y - 75 &&
		player1_y < player2_y + 30) {

		collision = true;
		image.draw(0, 0, 640, 480);              /// draws the image when collision is detected
		g = 255;                                /// sets the green channel 255, so it changes the default colour of players
		
		std::cout << collision << std::endl;   /// prints out the state of the logic
	}
	else {
		collision = false;
		g = 0;
	}


	///?? the syntax is not right here
	//// The logic for the player to bounce back if they collide
	/*if (collision = true && keyPressed = 'w') {
		yVelocity += 30;
	}
	if (collision = true && keyPressed = 's') {
		yVelocity -= 30;
	}
	if (collision = true && keyPressed = 'a') {
		xVelocity += 30;
	}
	if (collision = true && keyPressed = 'd') {
		xVelocity -= 30;
	}*/

		


	/// Logic for collision detection of the walls 
	if (xVelocity <= 0) {
		xVelocity = 0;
	}
	if (xVelocity >= 590) {
		xVelocity = 590;
	}
	if (yVelocity <= 0) {
		yVelocity = 0;
	}
	if (yVelocity >= 430) {
		yVelocity = 430;
	}



	//plane.drawWireframe(); 
    //texture.getTextureReference().bind(); ///to draw with texture
	// now draw filled...
	//plane.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	if (key == 'w') {
		yVelocity -= 5;
		player1_y -= 5;
	}
	else if (key == 's') {
		yVelocity += 5;
		player1_y += 5;
	}
	else if (key == 'a') {
		xVelocity -= 5;
		player1_x -= 5;
	}
	else if (key == 'd') {
		xVelocity += 5;
		player1_x += 5;     
	}
	else if (key == 'r') {
		player1_x = 0;
		player1_y = 0;
		
		xVelocity = 0;
		yVelocity = 0;

	}



}



bool ofApp::colide(ofPlanePrimitive a, ofPlanePrimitive b) {

	
	
	return 1;
}









//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
